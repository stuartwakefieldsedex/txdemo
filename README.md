# TxDemo

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/8774e615795c49dab420f5cacfcbf0cd)](https://www.codacy.com/bb/stuartwakefieldsedex/txdemo/dashboard?utm_source=stuartwakefieldsedex@bitbucket.org&amp;utm_medium=referral&amp;utm_content=stuartwakefieldsedex/txdemo&amp;utm_campaign=Badge_Grade)

Demonstrates issue seen with transaction retries in CockroachDB
and the alternative in PostgreSQL.

## Overview

[IntroTest:](./src/test/kotlin/txdemo/IntroTest.kt) shows the
default transaction isolation level for CockroachDB and PostgreSQL.

[DomainLogic:](./src/test/kotlin/txdemo/DomainLogic.kt) shows the
domain logic from our real-world codebase containing an invariant
we care to preserve.

[V1__tables:](./src/test/resources/tables-only/V1__tables.sql) shows
the respective schema in use.

[SimpleTest:](./src/test/kotlin/txdemo/SimpleTest.kt) shows
that the implementation on both databases under no parallel load
works as expected.

[ParallelNonConflictingTest:](./src/test/kotlin/txdemo/ParallelNonConflictingTest.kt)
shows the issue we were having in Cockroach under parallel load with
transaction retries and the solution.

[V2__indexes:](./src/test/resources/tables-and-indexes/V2__indexes.sql) shows
the indexes solving the transaction contention issue.

[ParallelConflictingTest:](./src/test/kotlin/txdemo/ParallelConflictingTest.kt)
shows the issue with PostgreSQL `READ_COMMITTED` isolation level when
preserving application level invariants under parallel load.

## Summary

CockroachDB IS NOT PostgreSQL

The takeaway is that `1 + 1 = 3` by default in PostgreSQL whereas
CockroachDB handles this correctly, but it is not pain-free.
