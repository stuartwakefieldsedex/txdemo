CREATE TABLE saq_site_modules(
    site_module_id UUID DEFAULT gen_random_uuid(),
    site_code TEXT,
    module_code TEXT
);
