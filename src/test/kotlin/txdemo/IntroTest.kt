package txdemo

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import javax.sql.DataSource

/**
 * Some introductory tests confirming isolation levels in Postgres
 * vs Cockroach
 */
class IntroTest {

    /**
     * Postgres by default uses read committed transaction isolation level
     * this allows for greater performance by trading away some isolation.
     * The most common phenomena are phantom and non-repeatable reads. In
     * later tests we will show serialization anomaly related to phantoms.
     */
    @Test
    fun `test postgres default transaction isolation level`() {
        Assertions.assertEquals("read committed", postgresConfig.toDataSource().getTransactionIsolationLevel())
    }

    /**
     * Cockroach can only be set to serializable transaction isolation level.
     * Based mostly on the Google Spanner whitepaper, Cockroach handles this
     * with a combination of clock synchronisation, optimistic concurrency
     * control and multi-version concurrency control. Like Google Spanner it
     * is a CP system, in that it will sacrifice availability for consistency.
     * However, availability is effectively high enough that it is resilient
     * in the face of failure.
     */
    @Test
    fun `test cockroach default transaction isolation level`() {
        Assertions.assertEquals("serializable", cockroachConfig.toDataSource().getTransactionIsolationLevel())
    }
}

/**
 * Return the transaction isolation level currently set for
 * the connection.
 * @receiver the connection to query
 * @return the transaction isolation level
 */
fun DataSource.getTransactionIsolationLevel(): String =
    connection.use { connection ->
        connection.prepareStatement("SELECT current_setting('transaction_isolation')").use { statement ->
            statement.executeQuery().use { resultSet ->
                resultSet.next()
                resultSet.getString(1)
            }
        }
    }
