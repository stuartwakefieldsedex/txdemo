package txdemo

import org.flywaydb.core.api.configuration.FluentConfiguration
import javax.sql.DataSource

/**
 * Sets up a datasource with the flyway migration including only
 * tables and no secondary indexes for testing.
 * @receiver the DataSource to apply the migration to
 * @return the DataSource receiver after having the migration applied
 */
fun DataSource.setupTablesOnly() =
    flywayMigration("classpath:tables-only")

/**
 * Sets up a datasource with the flyway migration including both
 * tables and indexes for testing.
 * @receiver the DataSource to apply the migration to
 * @return the DataSource receiver after having the migration applied
 */
fun DataSource.setupTablesAndIndexes() =
    flywayMigration("classpath:tables-and-indexes")

/**
 * Runs a flyway migration on the current datasource using the
 * location specified. For example:
 *
 * dataSource.flywayMigration("classpath:example")
 *
 * Will run the migrations in the example directory from classpath
 * resources.
 * @receiver the DataSource to apply the migration to
 * @param location the path of the migration to apply
 * @return the DataSource receiver after having the migration applied
 */
fun DataSource.flywayMigration(location: String) = apply {
    FluentConfiguration()
        .dataSource(this)
        .locations(location)
        .load()
        .migrate()
}
