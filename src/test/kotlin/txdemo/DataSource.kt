package txdemo

import org.postgresql.ds.PGSimpleDataSource
import java.time.Instant
import java.util.*
import javax.sql.DataSource

/**
 * The default log level to set when applying logging to
 * a DataSource
 */
const val DEFAULT_LOG_LEVEL = "TRACE"

/**
 * The default log file name for Cockroach DataSources
 */
const val DEFAULT_COCKROACH_LOG_FILE = "cockroach.log"

/**
 * The default log file name for Postgres DataSources
 */
const val DEFAULT_POSTGRES_LOG_FILE = "postgres.log"

/**
 * Creates a new logged DataSource including a new blank database
 * for Cockroach testing
 */
fun createCockroachDataSource() =
    cockroachConfig.createDataSource().logTo(DEFAULT_COCKROACH_LOG_FILE, DEFAULT_LOG_LEVEL)

/**
 * Creates a new logged DataSource including a new blank database
 * for Postgres testing
 */
fun createPostgresDataSource() =
    postgresConfig.createDataSource().logTo(DEFAULT_POSTGRES_LOG_FILE, DEFAULT_LOG_LEVEL)

/**
 * Creates a new DataSource pointing to a new blank database
 * @receiver the configuration to use for connection
 * @return the created DataSource pointing to the new database
 */
fun DataSourceConfig.createDataSource() =
    randomDatabaseName().let {
        toDataSource().createDatabase(it)
        toDataSource(it)
    }

/**
 * Creates a new database with the specified name.
 * @receiver the DataSource to use for connection
 * @param databaseName the name of the database to create
 */
fun DataSource.createDatabase(databaseName: String) {
    connection.use { connection ->
        connection.createStatement().use { statement ->
            statement.executeUpdate("""CREATE DATABASE "$databaseName"""")
        }
    }
}

/**
 * Generates a random database name for testing purposes.
 * @return the randomly generated database name
 */
fun randomDatabaseName() = "test-${Instant.now().epochSecond}-${UUID.randomUUID()}"

/**
 * Adds logging to the PGSimpleDataSource. Logs will be written
 * to the path specified at the given log level.
 * @receiver the PGSimpleDataSource on which to enable logging
 * @param file the path or name of the file to log to
 * @param level the log level to enable
 * @return the updated PGSimpleDataSource with logging
 */
fun PGSimpleDataSource.logTo(file: String, level: String) = apply {
    loggerFile = file
    loggerLevel = level
}
