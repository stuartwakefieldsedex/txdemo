package txdemo

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.lang.IllegalStateException
import javax.sql.DataSource

/**
 * The simple test shows that both databases are equivalent when
 * running under no contention. Both perform the domain logic and
 * protect the invariant.
 */
class SimpleTest {

    /**
     * Runs the test against Postgres showing that under no contention
     * the behaviour works as intended
     */
    @Test
    fun `test postgres runs logic`() {
        createPostgresDataSource().setupTablesOnly().simpleTest()
    }

    /**
     * Runs the test against Postgres showing that under no contention
     * Postgres is able to enforce our application-side invariant
     */
    @Test
    fun `test postgres runs logic protecting invariant`() {
        createPostgresDataSource().setupTablesOnly().simpleTestFailure()
    }

    /**
     * Runs the test against Cockroach showing that under no contention
     * the behaviour works as intended
     */
    @Test
    fun `test cockroach runs logic`() {
        createCockroachDataSource().setupTablesOnly().simpleTest()
    }

    /**
     * Runs the test against Cockroach showing that under no contention
     * Cockroach is able to enforce our application-side invariant
     */
    @Test
    fun `test cockroach runs logic protecting invariant`() {
        createCockroachDataSource().setupTablesOnly().simpleTestFailure()
    }
}

/**
 * Runs simple checks for the domain logic to ensure a site and module code
 * record get added to the database.
 * @receiver the DataSource to run the tests with
 * @throws AssertionError if tests fail
 */
fun DataSource.simpleTest() {
    connection.use { it.executeDomainLogic("ZS1", "ZQM1") }
    assertEquals(1, connection.use { it.getSiteModuleCount("ZS1", "ZQM1") })
}

/**
 * Runs simple checks for the domain logic to ensure the code protects the
 * invariants.
 * @receiver the DataSource to run the tests with
 * @throws AssertionError if the tests fail
 */
fun DataSource.simpleTestFailure() {
    connection.use { it.executeDomainLogic("ZS1", "ZQM1") }
    assertThrows<IllegalStateException> { connection.use { it.executeDomainLogic("ZS1", "ZQM1") } }
}
