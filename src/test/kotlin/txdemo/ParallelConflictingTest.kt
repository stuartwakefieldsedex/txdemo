package txdemo

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.postgresql.util.PSQLException
import java.sql.Connection
import javax.sql.DataSource
import kotlin.concurrent.thread
import kotlin.reflect.KClass

/**
 * However, when we introduce conflicting changes the story changes somewhat.
 * Postgres does not provide the same level of transaction isolation and in
 * this test we are able to break our application-side invariant.
 */
class ParallelConflictingTest {

    /**
     * In this test we are effectively able to bypass the application-side
     * checks due to the reduced level of isolation. Transactions can introduce
     * conflicting changes resulting in a serialization anomaly, where the
     * result cannot be reproduced by running the transactions sequentially in
     * any order.
     */
    @Test
    fun `test postgres runs logic concurrently`() {
        createPostgresDataSource().setupTablesAndIndexes().parallelConflictingTest()
    }

    /**
     * Cockroach uses a higher level of transaction isolation preserving
     * consistency within the database. Here we receive the same exception we
     * received before, however, this time it is for good reason:
     *
     * org.postgresql.util.PSQLException:
     * ERROR: restart transaction: TransactionRetryWithProtoRefreshError:
     * TransactionRetryError: retry txn (RETRY_SERIALIZABLE - failed preemptive refresh)
     */
    @Test
    fun `test cockroach runs logic concurrently`() {
        createCockroachDataSource().setupTablesAndIndexes().parallelConflictingTest()
    }

    /**
     * When we introduce automatic retries around our application code what
     * we end up with is the same result as if the tests had been running
     * sequentially. The application side invariant check raises the exception.
     */
    @Test
    fun `test cockroach runs logic concurrently with retries`() {
        createCockroachDataSource().setupTablesAndIndexes().parallelConflictingTestWithRetries()
    }
}

/**
 * Executes the domain logic in parallel threads but for the same
 * site and module code. This tests how the database handles concurrency.
 * In real scenarios this could happen with two requests performing
 * updates on the same record at the same time and might be spread
 * across backend instances.
 * @receiver the DataSource to run the test with
 * @throws AssertionError if the tests fail
 */
fun DataSource.parallelConflictingTest() =
    parallelConflictingTestOf { executeDomainLogic("ZS1", "ZQM1") }

/**
 * Executes the domain logic in parallel threads for the same site
 * and module code but also performs retries.
 * @receiver the DataSource to run the test with
 * @throws AssertionError if the tests fail
 */
fun DataSource.parallelConflictingTestWithRetries() =
    parallelConflictingTestOf { retry<PSQLException> { executeDomainLogic("ZS1", "ZQM1") } }

/**
 * Executes the specified block with two parallel threads and checks
 * our invariant still holds.
 * @receiver the DataSource to run the test with
 * @param block the block of statements to run as part of the test
 */
fun DataSource.parallelConflictingTestOf(block: Connection.() -> Unit) {
    val threads = (0..1).map { thread { connection.use(block) } }
    threads.forEach { it.join() }
    Assertions.assertEquals(1, connection.use { it.getSiteModuleCount("ZS1", "ZQM1") })
}

/**
 * Specify the number of retries to attempt. In our tests we only need
 * to retry the once but under higher levels of contention this may
 * want to be higher.
 */
const val DEFAULT_RETRY_COUNT = 1

/**
 * Retries the given block of statements the default number of retries
 * given we receive the parameterized exception type.
 * @receiver the connection on which to retry
 * @param block the block of statements to retry
 */
inline fun <reified E : Throwable> Connection.retry(noinline block: Connection.() -> Unit) =
    retry(E::class, DEFAULT_RETRY_COUNT, block)

/**
 * Retries the given block of statements the specified number of retries
 * given we receive the exception type expected.
 * @receiver the connection on which to retry
 * @param expectedType the expected exception type
 * @param times the number of times to retry
 * @param block the block of statements to retry
 */
fun Connection.retry(expectedType: KClass<out Throwable>, times: Int, block: Connection.() -> Unit) {
    return try {
        block()
    } catch (ex : Throwable) {
        if (expectedType.isInstance(ex) && times > 0) {
            retry(expectedType, times - 1, block)
        } else {
            throw ex
        }
    }
}
