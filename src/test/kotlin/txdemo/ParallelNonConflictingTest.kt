package txdemo

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import javax.sql.DataSource
import kotlin.concurrent.thread

/**
 * The parallel non-conflicting tests shows that Postgres performs better
 * than Cockroach for non-conflicting updates. Cockroach is sensitive to
 * consistency issues and we see a false positive where Cockroach believes
 * there to be transaction contention although the updates are indeed safe.
 */
class ParallelNonConflictingTest {

    /**
     * Postgres happily performs the parallel updates without issue and we
     * receive the results we would expect.
     */
    @Test
    fun `test postgres runs logic concurrently for non conflicting updates`() {
        createPostgresDataSource().setupTablesOnly().parallelNonConflictingTest()
    }

    /**
     * Cockroach on the other hand raises the following exception:
     *
     * org.postgresql.util.PSQLException:
     * ERROR: restart transaction: TransactionRetryWithProtoRefreshError:
     * TransactionRetryError: retry txn (RETRY_SERIALIZABLE - failed preemptive refresh)
     *
     * This is because our invariant that checks the count of the rows is
     * currently forced to perform a full table scan as there are no indexes
     * allowing the database to selectively look at the rows required by our
     * domain logic.
     */
    @Test
    fun `test cockroach runs logic concurrently for non conflicting updates`() {
        createCockroachDataSource().setupTablesOnly().parallelNonConflictingTest()
    }

    /**
     * By adding indexes we resolved the issue, now Cockroach is able to look
     * up the rows relevant to the operation efficiently without consulting other
     * rows. The two transactions are no longer in conflict with each other.
     */
    @Test
    fun `test cockroach runs logic concurrently for non conflicting updates with indexes`() {
        createCockroachDataSource().setupTablesAndIndexes().parallelNonConflictingTest()
    }
}

/**
 * Executes the domain logic in parallel threads but for different same
 * site and module code. In this case logically there isn't any contention,
 * however, depending on how the data is structured and indexed on the
 * database, contention can be created by requiring the database to scan
 * an entire table.
 * @receiver the datasource to run the test with
 * @throws AssertionError if the tests fail
 */
fun DataSource.parallelNonConflictingTest() {
    val threads = listOf(
        thread { connection.use { it.executeDomainLogic("ZS1", "ZQM1") } },
        thread { connection.use { it.executeDomainLogic("ZS2", "ZQM2") } }
    )
    threads.forEach { it.join() }
    Assertions.assertEquals(1, connection.use { it.getSiteModuleCount("ZS1", "ZQM1") })
    Assertions.assertEquals(1, connection.use { it.getSiteModuleCount("ZS2", "ZQM2") })
}
