package txdemo

import java.lang.IllegalStateException
import java.sql.Connection

/**
 * Executes the domain logic we are testing for a particular
 * site and module code.
 * @receiver the connection to use to run the domain logic
 * @param siteCode the site code to run the logic for
 * @param moduleCode the module code to run the logic for
 */
fun Connection.executeDomainLogic(siteCode: String, moduleCode: String) =
    transaction {
        val count = getSiteModuleCount(siteCode, moduleCode)
        if (count > 0L) {
            throw IllegalStateException("Site module already exists for site $siteCode and module $moduleCode")
        }
        insertSiteModule(siteCode, moduleCode)
    }

/**
 * Fetch the count of site code module code entries that match
 * the given site code and module code
 * @receiver the connection to use to fetch the records
 * @param siteCode the site code to match
 * @param moduleCode the module code to match
 * @return the count
 */
fun Connection.getSiteModuleCount(siteCode: String, moduleCode: String) =
    prepareStatement(
        """
        SELECT COUNT(1)
        FROM saq_site_modules
        WHERE site_code = ? AND module_code = ?
        """.trimIndent()
    ).use { statement ->
        statement.setString(1, siteCode)
        statement.setString(2, moduleCode)
        statement.executeQuery().use { resultSet ->
            resultSet.next()
            resultSet.getLong(1)
        }
    }

/**
 * Inserts a site code module code record into the database
 * @receiver the connection to perform the insert against
 * @param siteCode the site code value to insert
 * @param moduleCode the module code value to insert
 */
fun Connection.insertSiteModule(siteCode: String, moduleCode: String) {
    prepareStatement(
        """
        INSERT INTO saq_site_modules (site_code, module_code)
        VALUES (?, ?)
        """.trimIndent()
    ).use { statement ->
        statement.setString(1, siteCode)
        statement.setString(2, moduleCode)
        statement.execute()
    }
}

/**
 * Wraps the statements within the block within a single transaction.
 * It does this by switching into manual commit mode and explicitly
 * calling commit after the block has been executed.
 * @receiver the connection to run the transaction
 * @param block the code block to run within the transaction
 */
fun <T> Connection.transaction(block: Connection.() -> T) {
    autoCommit = false
    block()
    commit()
}
