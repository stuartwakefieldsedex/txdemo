package txdemo

import org.postgresql.ds.PGSimpleDataSource

/**
 * The default host to connect to. Defaulted to localhost
 * for our tests.
 */
const val DEFAULT_HOST = "localhost"

/**
 * The default port that Postgres listens on. Defaulted to
 * the standard Postgres default port of 5432 for our tests.
 */
const val DEFAULT_POSTGRES_PORT = 5432

/**
 * The default user that Postgres uses for auth. Defaulted to
 * the standard Postgres default user "postgres" for our tests.
 */
const val DEFAULT_POSTGRES_USER = "postgres"

/**
 * The default port that Cockroach listens on. Defaulted to
 * the standard Cockroach default port of 26257 for our tests.
 */
const val DEFAULT_COCKROACH_PORT = 26257

/**
 * The default user that Cockroach uses for auth. Defaulted to
 * the standard Cockroach default user "root" for our tests.
 */
const val DEFAULT_COCKROACH_USER = "root"

/**
 * The default application name for connections created in our
 * tests. This value commonly appears in the admin view for
 * the database indicating which application is executing
 * which statements.
 */
const val DEFAULT_APPLICATION_NAME = "txdemo"

/**
 * The DataSource configuration data class holding the properties
 * we are using to create connections to our databases.
 */
data class DataSourceConfig(
    val host: String,
    val port: Int,
    val user: String,
    val applicationName: String = DEFAULT_APPLICATION_NAME
)

/**
 * The configuration for connecting to Postgres
 */
val postgresConfig = DataSourceConfig(
    host = DEFAULT_HOST,
    port = DEFAULT_POSTGRES_PORT,
    user = DEFAULT_POSTGRES_USER
)

/**
 * The configuration for connecting to Cockroach
 */
val cockroachConfig = DataSourceConfig(
    host = DEFAULT_HOST,
    port = DEFAULT_COCKROACH_PORT,
    user = DEFAULT_COCKROACH_USER
)

/**
 * Creates a DataSource from the given configuration. Can optionally
 * pass a database name. Without specifying the database name we can
 * use this connection to create a new database for our tests.
 * @receiver the configuration from which to create a DataSource
 * @param databaseName the optional database name to connect to
 * @return the DataSource we can use to connect to the database
 */
fun DataSourceConfig.toDataSource(databaseName: String? = null) = PGSimpleDataSource().apply {
    serverNames = arrayOf(host)
    portNumbers = intArrayOf(port)
    user = this@toDataSource.user
    applicationName = this@toDataSource.applicationName
    this.databaseName = databaseName
}
